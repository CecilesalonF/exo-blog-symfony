<?php

namespace App\Repository;

use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Author::class);
    }

    // Méthode pour trouver les auteurs qui ont 1 ou plusieurs articles avec un mot choisi dans le titre
    public function findAuthorsByArticleTitle($word)
    {
        return $this->createQueryBuilder('a')
            // Jointure avec la tables Article
            ->leftJoin('a.articles', 'art')
            // Rajoute les articles au résultat de requête
            ->addSelect('art')
            // Qui ont dans leur titre le mot contenu dans la variable $word
            ->where('art.title LIKE :word' )
            ->setParameter('word', '%'.$word.'%')
            ->getQuery()
            ->getArrayResult();
    }
}
