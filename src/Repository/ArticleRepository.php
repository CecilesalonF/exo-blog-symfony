<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // Méthode qui récupère des articles selon un mot (entier ou non) dans le titre
    public function findByWord($word)
    {
        return $this->createQueryBuilder('a')
            ->where('a.title LIKE :word')
            ->setParameter('word', '%'.$word.'%')
            ->getQuery()
            ->getArrayResult();
    }

    // Méthode qui récupère des articles selon un mot (entier ou non) dans le titre
    public function findByType($word)
    {
        // Sélectionne automatiquement la table article (repository lié à l'entité)
        // "a" est un alias qui va pouvoir être utilisé dans la requête
        // createQueryBuilder est une méthode de la classe EntityManager qui permet de créer une requête
        // en utilisant le language PHP
        // Doctrine se chargera ensuite de traduire la requête dans la bonne syntaxe SQL
        // Elle est ici placée dans une variable "$qb"
        $qb = $this->createQueryBuilder('a');

        // Sélectionne la table qui a pour alias 'a'
        $query = $qb->select('a')
            // dont le titre contient l'alias :word
            ->where('a.title LIKE :word')
            // l'alias 'word' prend alors la valeur de la variable $word
            // quelque soit ce qu'il y a avant et après le mot
            ->setParameter('word', '%'.$word.'%')
            // récupère la requête
            ->getQuery();

        // Déclare que le résultat de la requête est un tableau
        $result = $query->getArrayResult();

        // Retourne le résultat
        return $result;
    }

    // Méthode qui récupère des articles selon un mot (entier ou non) dans le titre
    // function to get 2 lasts articles
    public function find2lastsArticles()
    {
        return $this->createQueryBuilder('a')
            ->setMaxResults(2)
            ->join('a.author', 'aut')
            ->addSelect('aut')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }
}
