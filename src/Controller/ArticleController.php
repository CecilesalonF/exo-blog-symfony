<?php
/**
 * Created by PhpStorm.
 * User: lapiscine
 * Date: 19/07/2019
 * Time: 21:11
 */

namespace App\Controller;


use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{

    /**
     * @Route("/articles", name="articles")
     */
    public function authors(ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->findAll();

        return $this->render('articles/all_articles.html.twig', [
            'articles' => $articles
        ]);
    }


    /**
     * @Route("/article/{id}", name="article")
     * Méthode qui retourne un seul article qui a pour id la valeur de la wildcard {id}
     */
    public function article(ArticleRepository $articleRepository, $id)
    {
        $article = $articleRepository->find($id);

        return $this->render('articles/single_article.html.twig', [
            'article' => $article
        ]);
    }


    /**
     * @Route("/article", name="article_title")
     * Méthode qui retourne tous les articles ayant dans leur titre le mot 'word'
     */
    public function articleByTitle(ArticleRepository $articleRepository, Request $request)
    {
        // Récupère le paramètre de la requête (dans l'url : ?word=votreMot)
        $word = $request->query->get('word');

        // Récupère les articles qui ont dans leur titre $word
        // grâce à la méthode créée précédemment dans Article Repository
        $articles = $articleRepository->findByWord($word);

        // Retourne un fichier twig + le tableau d'articles
        return $this->render('articles/all_articles.html.twig', [
            'articles' => $articles
        ]);
    }
}