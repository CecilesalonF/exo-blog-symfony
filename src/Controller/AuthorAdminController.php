<?php
/**
 * Created by PhpStorm.
 * User: lapiscine
 * Date: 19/07/2019
 * Time: 21:01
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\Author;
use App\Form\AuthorType;
use App\Repository\ArticleRepository;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AuthorAdminController extends AbstractController
{
    /**
     * @Route("/admin/new/author", name="new_author")
     * Méthode pour créer un nouvel auteur via un formulaire, au chargement de la page
     */
    public function newAuthor(EntityManagerInterface $entityManager, Request $request)
    {
        // Instancie l'entité Author
        $author = new Author();

        // Crée le formulaire grâce à la méthode "createForm"
        // dans laquelle on passe la représentation abstraite du formulaire (objet)
        // + l'instance de l'entité Author
        $form = $this->createForm(AuthorType::class, $author);

        // Récupère la requête uniquement si la méthode du formulaire est "post"
        $form->handleRequest($request);

        // Si le formulaire est envoyé et qu'il est valide (si les champs obligatoires sont remplis...)
        // si ce n'est pas le cas, cette étape est sautée pour arriver directement au return
        // (donc l'affichage de la page avec le formulaire)
        if ($form->isSubmitted() && $form->isValid()) {
            // On envoie l'article en base de données grâce aux méthodes persist(objet) + flush
            // persist + flush est l'équivalent de commit + push de Git.
            $entityManager->persist($author);
            $entityManager->flush();
            $this->addFlash('Success', 'L\'auteur '. $author->getFirstname(). ' ' . $author->getLastname() . ' a bien été enregistré.');
        } else {
            $this->addFlash('Fail', 'L\'auteur '. $author->getFirstname(). ' ' . $author->getLastname() . ' n\'a pas été 
            enregistré, veuillez réessayer.');
        }

        // Retourne un fichier twig avec un formulaire
        return $this->render('authors/form_author.html.twig', [
            // createView retourne tout le code html correspondant au formulaire
            'authorForm' => $form->createView()
        ]);
    }


    /**
     * @Route("/admin/remove/author/{id}", name="remove_author")
     * Méthode pour supprimer un auteur dont l'id correspond à la wildcard {id} au chargement de la page
     */
    public function removeAuthor(EntityManagerInterface $entityManager, $id, AuthorRepository $authorRepository,
                                 Request $request)
    {
        // Récupère l'auteur qui a pour id la wildcard {id}
        $author = $authorRepository->find($id);

        // Première étape avant le flush (comme pour git : commit + push)
        // (Dit à Doctrine que vous voulez éventuellement supprimer l'auteur en question --> pas encore de requête)
        $entityManager->remove($author);

        // Exécute la requête
        $entityManager->flush();

        $this->addFlash('Success', 'L\'auteur '. $author->getFirstname(). ' ' . $author->getLastname() . ' a bien été enregistré.');

        // Rafraîchit la page
        return $this->redirect($request->getUri());
    }


    /**
     * @Route("/admin/update/author/{id}", name="update_author")
     * Méthode pour modifier un auteur dont l'id correspond à la wildcard {id} au chargement de la page
     */
    public function updateArticle(EntityManagerInterface $entityManager, $id, AuthorRepository $authorRepository,
                                  Request $request)
    {
        // Récupère l'auteur qui a pour id la wildcard {id}
        $author = $authorRepository->find($id);

        // Crée le formulaire grâce à la méthode "createForm"
        // dans laquelle on passe la représentation abstraite du formulaire (objet)
        // + l'auteur de la variable $article
        $form = $this->createForm(AuthorType::class, $author);

        // Récupère la requête uniquement si la méthode du formulaire est "post"
        $form->handleRequest($request);

        // Si le formulaire est envoyé et qu'il est valide (si les champs obligatoires sont remplis...)
        // si ce n'est pas le cas, cette étape est sautée pour arriver directement au return
        // (donc l'affichage de la page avec le formulaire)
        if ($request->isMethod('Post')) {
            // On envoie l'article en base de données grâce aux méthodes persist(objet) + flush
            // persist + flush est l'équivalent de commit + push de Git.
            $entityManager->persist($author);
            $entityManager->flush();

            // renvoie vers la page 'authors'
            return $this->redirectToRoute('authors');
        }

        // Retourne un fichier twig avec un formulaire
        return $this->render('authors/form_author.html.twig', [
            // createView retourne tout le code html correspondant au formulaire
            'authorForm' => $form->createView()
        ]);
    }

}