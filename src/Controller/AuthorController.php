<?php
/**
 * Created by PhpStorm.
 * User: lapiscine
 * Date: 08/07/2019
 * Time: 17:53
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\Author;
use App\Repository\ArticleRepository;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class AuthorController extends AbstractController
{

    /**
     * @Route("/authors", name="authors")
     */
    public function authors(AuthorRepository $authorRepository)
    {
        $authors = $authorRepository->findAll();

        return $this->render('authors/all_authors.html.twig', [
            'authors' => $authors
        ]);
    }

    /**
     * @Route("/author/{id}", name="author")
     * Méthode qui retourne un seul auteur qui a pour id la valeur de la wildcard {id}
     */
    public function author(AuthorRepository $authorRepository, $id)
    {
        //$authorRepository = new AuthorRepository();
        $author = $authorRepository->find($id);

        return $this->render('authors/single_author.html.twig', [
            'author' => $author
        ]);
    }

    /**
     * @param AuthorRepository $authorRepository
     * @return Response
     * @Route("/authors/article/title", name="authors_with_article_title")
     */
    public function authorsWithArticleTitle(AuthorRepository $authorRepository)
    {
        $word = 'Djessica';

        //$authorRepository = new AuthorRepository();
        $authors = $authorRepository->findAuthorsByArticleTitle($word);

        return $this->render('authors/all_authors.html.twig', [
            'authors' => $authors
        ]);
    }


}