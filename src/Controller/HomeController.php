<?php
/**
 * Created by PhpStorm.
 * User: lapiscine
 * Date: 19/07/2019
 * Time: 20:59
 */

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->find2lastsArticles();

        return $this->render('homepage.html.twig', [
            'articles' => $articles
        ]);
    }

}