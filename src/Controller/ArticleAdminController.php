<?php
/**
 * Created by PhpStorm.
 * User: lapiscine
 * Date: 19/07/2019
 * Time: 21:07
 */

namespace App\Controller;


use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class ArticleAdminController extends AbstractController
{

    /**
     * @Route("/admin/new/article", name="new_article")
     * Méthode pour créer un nouvel article via un formulaire, au chargement de la page
     */
    public function newArticle(EntityManager $entityManager, Request $request, ValidatorInterface $validator)
    {
        // Instancie l'entité Article
        $article = new Article();

        // Crée le formulaire grâce à la méthode "createForm"
        // dans laquelle on passe la représentation abstraite du formulaire (objet)
        // + l'instance de l'entité Article
        $form = $this->createForm(ArticleType::class, $article);

        // Récupère la requête uniquement si la méthode du formulaire est "post"
        $form->handleRequest($request);

        // Si le formulaire est envoyé et qu'il est valide (si les champs obligatoires sont remplis...)
        // si ce n'est pas le cas, cette étape est sautée pour arriver directement au return
        // (donc l'affichage de la page avec le formulaire)
        if ($form->isSubmitted() && $form->isValid()) {
            // On envoie l'article en base de données grâce aux méthodes persist(objet) + flush
            // persist + flush est l'équivalent de commit + push de Git.
            $entityManager->persist($article);
            $entityManager->flush();
            $this->addFlash('Success', 'L\'article "'. $article->getTitle() . '" a bien été enregistré.');

            // Redirige vers la route 'articles'
            return $this->redirectToRoute('articles');

        } else {
            $this->addFlash('Fail', 'L\'article '. $article->getTitle() . ' n\'a pas été 
            enregistré, veuillez réessayer.');
        }

        // Retourne un fichier twig avec un formulaire
        return $this->render('articles/form_article.html.twig', [
            // createView retourne tout le code html correspondant au formulaire
            'articleForm' => $form->createView()
        ]);
    }


    /**
     * @Route("/admin/remove/article/{id}", name="remove_article")
     * Méthode pour supprimer un article dont l'id correspond à la wildcard {id} au chargement de la page
     */
    public function removeArticle(EntityManager $entityManager, $id, ArticleRepository $articleRepository)
    {
        // Récupère l'article qui a pour id la wildcard {id}
        $article = $articleRepository->find($id);

        // Première étape avant le flush (comme pour git : commit + push)
        // (Dit à Doctrine que vous voulez éventuellement supprimer l'article en question --> pas encore de requête)
        $entityManager->remove($article);

        // Exécute la requête
        $entityManager->flush();

        $this->addFlash('Success', 'L\'article '. $article->getTitle() . ' a bien été supprimé.');

        // Redirige vers la route 'articles'
        return $this->redirectToRoute('articles');

    }


    /**
     * @Route("/admin/update/article/{id}", name="update_article")
     * Méthode pour modifier un article dont l'id correspond à la wildcard {id} au chargement de la page
     */
    public function updateArticle(EntityManager $entityManager, $id, ArticleRepository $articleRepository,
                                  Request $request)
    {
        // Récupère l'article qui a pour id la wildcard {id}
        $article = $articleRepository->find($id);

        // Crée le formulaire grâce à la méthode "createForm"
        // dans laquelle on passe la représentation abstraite du formulaire (objet)
        // + l'article de la variable $article
        $form = $this->createForm(ArticleType::class, $article);

        // Récupère la requête uniquement si la méthode du formulaire est "post"
        $form->handleRequest($request);

        // Si le formulaire est envoyé et qu'il est valide (si les champs obligatoires sont remplis...)
        // si ce n'est pas le cas, cette étape est sautée pour arriver directement au return
        // (donc l'affichage de la page avec le formulaire)
        if ($request->isMethod('Post')) {
            // On envoie l'article en base de données grâce aux méthodes persist(objet) + flush
            // persist + flush est l'équivalent de commit + push de Git.
            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('Success', 'L\'article "'. $article->getTitle() . '" a bien été modifié.');

            // Redirige vers la route 'articles'
            return $this->redirectToRoute('articles');

        } else {
            $this->addFlash('Fail', 'L\'article '. $article->getTitle() . ' n\'a pas été 
            modifié, veuillez réessayer.');
        }

        // Retourne un fichier twig avec un formulaire
        return $this->render('articles/form_article.html.twig', [
            // createView retourne tout le code html correspondant au formulaire
            'articleForm' => $form->createView()
        ]);
    }

}